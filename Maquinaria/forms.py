from django import forms

class MaquinaForm(forms.Form):
    patente =  forms.CharField(label='Patente', help_text="Patente con el siguiente formato XX-XX-11")