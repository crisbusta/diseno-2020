from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from Maquinaria.models import Maquina
from Maquinaria.forms import MaquinaForm
from django.db import IntegrityError

@method_decorator(login_required, name='dispatch')
class MaquinaListView(ListView):
    model = Maquina
    template_name = 'listado_maquina.html'
    context_object_name = 'maquinas'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(MaquinaListView, self).get_context_data(**kwargs)
        maquinas = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(maquinas, self.paginate_by)
        try:
            maquinas = paginator.page(page)
        except PageNotAnInteger:
            maquinas = paginator.page(1)
        except EmptyPage:
            maquinas = paginator.page(paginator.num_pages)
        context['maquinas'] = maquinas
        return context

@method_decorator(login_required, name='dispatch')
class MaquinaCreateView(CreateView):
    model = Maquina
    template_name = 'create.html'
    fields = ('modelo', 'agno', 'patente', 'estado')
    success_url = reverse_lazy('maquinaria:maquina-list')

@method_decorator(login_required, name='dispatch')
class MaquinaDetailView(DetailView):
    model = Maquina
    template_name = 'detail.html'
    context_object_name = 'maquina'

@method_decorator(login_required, name='dispatch')
class MaquinaUpdateView(UpdateView):

    model = Maquina
    template_name = 'update.html'
    context_object_name = 'maquina'
    fields = ('modelo', 'agno', 'patente', 'estado')

    def get_success_url(self):
        return reverse_lazy('maquinaria:maquina-detail', kwargs={'pk': self.object.id})

def MaquinaDeleteView(request, id):
    data = {}
    template_name = 'delete.html'
    data['maquina'] = Maquina.objects.get(pk=id)
    if request.method == 'POST':
        data['maquina'].estado = 'BL'
        data['maquina'].save()
        return redirect('maquinaria:maquina-list')
    
    return render (request, template_name, data)

def searchMaquina(request):
    data = {}
    template_name = 'patente-consulta.html'
    data['form'] = MaquinaForm(request.POST or None)

    if request.method == 'POST':
        if data['form'].is_valid():
            try:
                pat = request.POST['patente']
                maquina = Maquina.objects.get(patente=pat)
                
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Consulta realizada con exito'
                )
                return redirect('maquinaria:maquina-detail', pk=maquina.id)
            except ObjectDoesNotExist:
                maquina = None
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Rut no registra multas',
                )

            except IntegrityError as ie:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Problemas al realizar la consulta: {error}.'.format(
                        error=str(ie))
                )
            
        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Problemas al realizar la consulta',
            )
        
    return render(request, template_name, data)


def Home(request):
    data = {}
    template_name = 'home.html'

    return render(request, template_name, data)