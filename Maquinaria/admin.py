from django.contrib import admin
from Maquinaria.models import Maquina

@admin.register(Maquina)
class MaquinaAdmin(admin.ModelAdmin):
    list_display = ['patente', 'agno', 'modelo']
# Register your models here.
