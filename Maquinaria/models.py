from django.db import models

class Maquina(models.Model):
    ESTADO = (
        ('BL', 'BLOQUEADA'),
        ('MA', 'MANTENCION'),
        ('DI', 'DISPONIBLE'),
        ('AR', 'ARRENDADA'),
    )

    Modelos = (
        ('903', '903D'),
        ('906', '906M'),
        ('AG', 'Ag Handler 906M'),
        ('AH', 'Ag Handler 907M'),
    )

    modelo = models.CharField(max_length=50, choices=Modelos)
    patente = models.CharField(max_length=150, unique=True)
    agno = models.PositiveSmallIntegerField()
    estado = models.CharField(max_length=50, choices=ESTADO, default='DI')


    def __str__(self):
        return self.modelo
    
    def estado_verbose(self):
        return dict(Maquina.ESTADO)[self.estado]

    def modelo_verbose(self):
        return dict(Maquina.Modelos)[self.modelo]

#bloqueada - mantencion - disponible - arrendada