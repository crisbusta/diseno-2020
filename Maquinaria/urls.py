from django.urls import path
from Maquinaria import views
app_name = 'maquinaria'


urlpatterns = [
    path('home', views.Home, name='home'),
    path('maquina', views.MaquinaListView.as_view(), name='maquina-list'),
    path('maquina/create', views.MaquinaCreateView.as_view(), name='maquina-create'),
    path('maquina/buscar', views.searchMaquina, name='maquina-search'),
    path('maquina/<int:pk>', views.MaquinaDetailView.as_view(), name='maquina-detail'),   
    path('maquina/<int:pk>/update', views.MaquinaUpdateView.as_view(), name='maquina-update'), 
    path('maquina/<int:id>/delete', views.MaquinaDeleteView, name='maquina-delete'),
]